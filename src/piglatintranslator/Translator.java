package piglatintranslator;

public class Translator {

	public static final String NIL = "nil";
	
	private String phrase;
	private String phrase2 = "";
	
	
	public Translator(String inputPhrase) {
		
		phrase = inputPhrase;
		
	}

	public String getPhrase() {
		// TODO Auto-generated method stub
		return phrase;
	}

	public String translate() {
		// TODO Auto-generated method stub
		
		if(phrase.isEmpty()) {
			
			return NIL;
			
		} 
		
		if(phrase.contains(" ")) {
			
			String [] vet= phrase.split(" ");
			int num_words = vet.length;
			Translator translator;
			
			for(int i=0;i<num_words-1;i++) {
				
				translator = new Translator(vet[i]);
				phrase2 = phrase2 + translator.translate() + " ";
			}
			
			translator = new Translator(vet[num_words-1]);
			phrase2 = phrase2 + translator.translate();
			return phrase2;
			
		}else {
			
			if(phrase.contains("-")) {
				
				String [] vet= phrase.split("-");
				int num_words = vet.length;
				Translator translator;
				
				for(int i=0;i<num_words-1;i++) {
					
					translator = new Translator(vet[i]);
					phrase2 = phrase2 + translator.translate() + "-";
				}
				
				translator = new Translator(vet[num_words-1]);
				phrase2 = phrase2 + translator.translate();
				return phrase2;
			}
		}
		
		
		if(startWithVowel()) {
			
			if(ContainingPunctuations()) {
				
				char [] vet = phrase.toCharArray();
				char punctuation = phrase.charAt(phrase.length()-1);
				phrase2 = phrase.substring(0,phrase.length()-1);
				
				
				if(phrase2.endsWith("y")) {
					
					phrase2 = phrase2 + "nay" + punctuation;
					
					return phrase2;
					
				}else if(endWithVowel2()) {
					
					phrase2 = phrase2 + "yay" + punctuation;
					
					return phrase2;
					
				}else if(!endWithVowel2()) {
					
					phrase2 = phrase2 + "ay" + punctuation;
					
					return phrase2;
					
				}
				
			}else if(phrase.endsWith("y")) {
				
				return phrase + "nay";
				
			}else if(endWithVowel()) {
				
				return phrase + "yay";
				
			}else if(!endWithVowel()) {
				
				return phrase + "ay";
				
			}
			
		} else if(!startWithVowel() && startWithSingleConsonant()) {
			
			char [] vet = phrase.toCharArray();
			
			if(ContainingPunctuations()) {
				
				char punctuation = phrase.charAt(phrase.length()-1);
				phrase2 = phrase.substring(0,phrase.length()-1);
				phrase2 = phrase2.substring(1) + vet[0] + "ay";
				
				return phrase2 = phrase2 + punctuation;
				
			}else{
			
			phrase2 = phrase.substring(1) + vet[0] + "ay";
			
			return phrase2;
			
			}
			
		} else if(!startWithVowel() && !startWithSingleConsonant()) {
			
			int count = 1;
			boolean check = false;
			
			do {
				
				if(!startWithMoreConsonant(count)) {
					
					count++;
					
				}else {
					
					check = true;
					
				}
				
			}while(check = false);
			
			char [] vet = phrase.toCharArray();
			phrase2 = phrase.substring(count);
			
			for(int j=0;j<count;j++) {
				
				phrase2 = phrase2 + vet[j];
				
			}
			
			phrase2 = phrase2 + "ay";
			return phrase2;
			
		}
		
		return phrase2;
		
	}
	
	private boolean startWithVowel() {
		
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u");
			
	}
	
	private boolean endWithVowel() {
		
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");
			
	}
	
	private boolean startWithSingleConsonant() {
		
		return phrase.startsWith("a",1) || phrase.startsWith("e",1) || phrase.startsWith("i",1) || phrase.startsWith("o",1) || phrase.startsWith("u",1);
		
	}
	
	private boolean startWithMoreConsonant(int i) {
		
		return phrase.startsWith("a",i) || phrase.startsWith("e",i) || phrase.startsWith("i",i) || phrase.startsWith("o",i) || phrase.startsWith("u",i);
		
	}
	
	private boolean ContainingPunctuations() {
		
		return phrase.endsWith(".") || phrase.endsWith(",") || phrase.endsWith(";") || phrase.endsWith(":") || phrase.endsWith("?") || phrase.endsWith("!") || phrase.endsWith("'") || phrase.endsWith("(") || phrase.endsWith(")");
		
	}

	
	private boolean endWithVowel2() {
		
		return phrase2.endsWith("a") || phrase2.endsWith("e") || phrase2.endsWith("i") || phrase2.endsWith("o") || phrase2.endsWith("u");
			
	}
}
